﻿using System;
using System.IO;
using System.Linq;
using cab201_dna_search;

namespace cab201_dna_search
{
    class Program
    {
        static void Main(string[] args)
        {
            ArgsParse(args);
        }

        // Parses each user provided argument
        static void ArgsParse(string[] args)
        {
            // Check if user has inputted enough arguments
            if (!(args.Length >= 3))
            {
                Console.WriteLine("Not enough arguments provided.");
                return;
            }

            // Use functions to check if level and file arguments are valid
            int level = ParseLevel(args[0]);
            string file = ParseFile(args[1]);

            // Try catch to check if the additional arguments provided are valid
            Console.WriteLine("Processing...");
            var process = new Process(file, level, args.Skip(2).ToArray());
            Console.WriteLine("Done!");
        }

        // Parse Level argument
        static int ParseLevel(string arg)
        {
            // Checks if argument starts with -level
            if (!arg.StartsWith("-level"))
            {
                Console.WriteLine("Invalid level format. Please specify a level with '-levelN', with N replaced with a number.");
                return 0;
            }

            // Checks to see if level is a valid integer
            int level;
            int.TryParse(arg.Substring(6), out level);
            return level;
        }

        // Parse file name argument
        static string ParseFile(string arg)
        {
            // Checks if supplied file name exists
            if (!File.Exists(arg))
            {
                Console.WriteLine("Specified file does not exist.");
                return null;
            }

            // Checks if file extention is .fasta
            if (!arg.EndsWith(".fasta"))
            {
                Console.WriteLine("Invalid file. Please specify a valid '.fasta' file.");
                return null;
            }
            return arg;
        }
    }
}
