using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace cab201_dna_search
{
    class Process
    {
        // Initialise instance variables
        private string sequencesFile;

        // Sets file variable upon object creation
        public Process(string sequencesFile, int level, string[] args)
        {
            this.sequencesFile = sequencesFile;
            
            var levels = new Dictionary<int, Action>();
            
            try
            {
                /* Use the Level function that corresponds with the level provided.
                Each case corresponds with the level number given by the level argument*/
                switch(level)
                {
                    case 1:
                        if (args.Length == 2) { Level1(Int32.Parse(args[0]), Int32.Parse(args[1])); return; }
                        break;
                    case 2:
                        if (args.Length == 1) { Level2(args[0]); return; }
                        break;
                    case 3:
                        if (args.Length == 2) { Level3(args[0], args[1]); return; }
                        break;
                    case 4:
                        if (args.Length == 3) { Level4(args[0], args[1], args[2]); return; }
                        break;
                    case 5:
                        if (args.Length == 1) { Level5(args[0]); return; }
                        break;
                    case 6:
                        if (args.Length == 1) { Level6(args[0]); return; }
                        break;
                    case 7:
                        if (args.Length == 1) { Level7(args[0]); return; }
                        break;
                    default:
                        Console.WriteLine("Invalid level number. Please specify a level with '-levelN', with N replaced with a number.");
                        return;
                }
                Console.WriteLine("Incorrect number of arguments provided.");
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Incorrect arguments provided.");
            }            
        }

        // Outputs specified amount of lines starting from a specified line
        public void Level1(int line, int amount)
        {
            // Check if line number is too low
            if (line < 1)
            {
                Console.WriteLine("Selected line cannot be below 1.");
                return;
            }

            // Take range of lines from file
            IEnumerable<string> values = File.ReadLines(this.sequencesFile).Skip(line - 1).Take(amount * 2);
            string displayValues = string.Join("\n", values);

            // Output if exists or display error if selected range returns nothing (range too high)
            if (values.Count() == 0)
            {
                Console.WriteLine("Selected value is out of range.");
                return;
            }
            Console.WriteLine($"{displayValues}");
        }

        // Outputs sequence lines that contains a specified sequence ID
        public void Level2(string id)
        {
            // Use a StreamReader to seek through a file
            using (StreamReader sr = new StreamReader(this.sequencesFile))
            {
                string line = null;
                int counter = 0;

                // Loop while line exists in file
                while ((line = sr.ReadLine()) != null)
                {
                    counter++;

                    // Output sequence if ID is found where an ID should be
                    if (counter % 2 != 0 && line.Substring(1,11) == id)
                    {
                        Console.WriteLine(line);
                        line = sr.ReadLine();
                        Console.WriteLine(line);
                        return;
                    }
                }

                // Display error if ID not found
                Console.WriteLine("Sequence ID not found.");
                return;
            }
        }

        // Outputs sequences lines to file that matches the sequence IDs provided in another file
        public void Level3(string queries, string output)
        {
            // Save query file to memory if it exists
            IEnumerable<string> queriesList;
            try
            {
                queriesList = File.ReadLines(queries);
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("Error, query file not found.");
                return;
            }
            
            var outputList = new List<string>();
            bool found;

            // Loop through each sequence ID in a file
            foreach (string sequence in queriesList)
            {
                found = false;
                // Use a StreamReader to seek through a file
                using (StreamReader sr = new StreamReader(this.sequencesFile))
                {
                    string line = null;
                    int counter = 0;

                    // Loop while line exists in file
                    while ((line = sr.ReadLine()) != null)
                    {
                        counter++;

                        // Add sequence to list if found where an ID should be
                        if (counter % 2 != 0 && line.Substring(1,11) == sequence)
                        {
                            outputList.Add(line);
                            line = sr.ReadLine();
                            outputList.Add(line);
                            found = true;
                            break;
                        }
                    }

                    // Display error if ID wasn't found
                    if (found == false)
                    {
                        Console.WriteLine($"Error, sequence {sequence} not found.");
                    }
                }
            }
            // Write all values in list to the specified output file
            File.WriteAllLines(output, outputList, Encoding.UTF8);
        }

        // Outputs sequence lines to file using an index file that links to each sequence ID
        public void Level4(string index, string queries, string output)
        {
            // Save index and query file to memory if either exists
            IEnumerable<string> indexList;
            IEnumerable<string> queriesList;
            try
            {
                indexList = File.ReadLines(index);
               
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("Error, index file not found.");
                return;
            }
            try
            {
                queriesList = File.ReadLines(queries);
               
            }
            catch (System.IO.FileNotFoundException)
            {
                Console.WriteLine("Error, query file not found.");
                return;
            }

            var outputList = new List<string>();

            // Loop through each sequence ID in a file
            foreach (string sequence in queriesList)
            {
                // Check if sequence ID exists by querying index
                string indexSequence = indexList.FirstOrDefault(str => str.Contains(sequence));
                if(string.IsNullOrEmpty(indexSequence))
                {
                    Console.WriteLine("Error, sequence " + sequence + " not found.");
                    continue;
                }

                // Use a StreamReader to seek through a file
                using (StreamReader sr = new StreamReader(this.sequencesFile))
                {
                    int position = Int32.Parse(indexSequence.Substring(13));
                    sr.BaseStream.Seek(position, SeekOrigin.Begin);

                    // Take two lines from the current position
                    for (int i = 0; i < 2; i++)
                    {
                        string value = sr.ReadLine();
                        outputList.Add(value);
                    }
                }
            }
            
            // Write all values in list to the specified output file
            File.WriteAllLines(output, outputList, Encoding.UTF8);
        }

        // Outputs specific sequences that contains a DNA string
        public void Level5(string dna)
        {
            // Use a StreamReader to seek through a file
            using (StreamReader sr = new StreamReader(this.sequencesFile))
            {
                // Set variables for getting current line, previous line, and Regex
                string line = null;
                string prevLine = null;
                bool found = false;
                Regex rx = new Regex(@">NR_[0-9]+.[0-9]");

                // Check one line at a time
                while ((line = sr.ReadLine()) != null) 
                {
                    // Check if line contains query and fetch previous line, using a regex to fetch only the ID
                    if (line.Contains(dna))
                    {
                        MatchCollection matches = rx.Matches(prevLine);

                        // Output results of regex
                        foreach (Match match in matches)
                        {
                            Console.WriteLine(match.ToString().Substring(1));
                            found = true;
                        }
                    }

                    // Save current line to be used as a previous line next iteration
                    prevLine = line;
                }

                // Display error if DNA sequence not found
                if (found == false)
                {
                    Console.WriteLine("Error, DNA query not found.");
                }
            }
        }

        // Outputs sequences that contain a meta-data word
        public void Level6(string metadata)
        {
            // Use a StreamReader to seek through a file
            using (StreamReader sr = new StreamReader(this.sequencesFile))
            {
                // Set line, counter, and Regex variables
                string line = null;
                int counter = 0;
                bool found = false;
                Regex rx = new Regex(@">NR_[0-9]+.[0-9]");

                // Loop while line exists in file
                while ((line = sr.ReadLine()) != null)
                {
                    counter++;
                    // Check if every odd line contains metadata query
                    if (line.Substring(12).Contains($" {metadata} ") && counter % 2 != 0)
                    {
                        MatchCollection matches = rx.Matches(line);

                        // Output results of regex
                        foreach (Match match in matches)
                        {
                            Console.WriteLine(match.ToString().Substring(1));
                            found = true;
                        }
                    }
                }

                // Display error if DNA sequence not found
                if (found == false)
                {
                    Console.WriteLine("Error, DNA query not found.");
                }
            }
        }

        // Outputs DNA sequences using wildcards
        public void Level7(string dna)
        {
            // Use a StreamReader to seek through a file
            using (StreamReader sr = new StreamReader(this.sequencesFile))
            {
                // Set line, counter, and Regex variables
                bool check = false;
                string line = null;
                string prevLine = null;
                int counter = 0;
                Regex rx = new Regex(@">NR_[0-9]+.[0-9]");
                Regex dnaRx = new Regex(@"" + dna.Replace("*", "[ACGT]*"));
                
                // Loop while line exists in file
                while ((line = sr.ReadLine()) != null)
                {
                    counter++;
                    // Check if every second line contains metadata query
                    if (counter % 2 == 0)
                    {
                        MatchCollection matches = dnaRx.Matches(line);

                        // Output results of entire sequence
                        if(matches.Any())
                        {
                            check = true;
                            Console.WriteLine($"{prevLine}\n{line}");
                        }
                    }
                    // Save current line to be used as a previous line next iteration
                    prevLine = line;
                }

                // Display error if DNA sequence not found
                if (check == false)
                {
                    Console.WriteLine("Error, no matches found.");
                }
            }
        }
    }
}